package com.slabs.kaizenemployee;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Sreekanth Putta on 02-11-2016.
 */
public class recentComplaints extends AppCompatActivity {
    Menu menu;
    String url;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recentcomplaints);
        assert getSupportActionBar()!=null;
        getSupportActionBar().setTitle("Recent Complaints");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayout f = (LinearLayout) findViewById(R.id.linearrecentcomplaints);
        try{
            f.removeAllViews();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        SharedPreferences getSharedPreferences;
        getSharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        url = getSharedPreferences.getString("URL","192.168.42.29");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        new refreshRecentComplaints().execute();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            new refreshRecentComplaints().execute();
            return true;
        }
        if (id == R.id.action_recentcomplaints) {
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class refreshRecentComplaints extends AsyncTask<Void,Void,Void>{

        MenuItem refreshItem = menu.findItem(R.id.action_refresh);
        String result = "";
        String employeeName, employeePin, memberId;
        boolean connection=false;
        JSONObject json_data;
        CharSequence w="";

        @Override
        protected Void doInBackground(Void... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            SharedPreferences getSharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
            String sqlcommand = "SELECT * FROM complaint WHERE `Complained by`='"+getSharedPreferences.getString("Employee Pin","0")+"' OR `Type`='Public' ORDER BY id DESC";
            nameValuePairs.add(new BasicNameValuePair("sqlcommand",sqlcommand));

            StrictMode.setThreadPolicy(policy);

            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url+"/kaizen/accesssql.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;
                Log.e("log_tag", "connection success ");
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error in http connection "+e.toString());
            }
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null){
                    sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
            }
            catch(Exception e) {
                Log.e("log_tag", "Error converting result "+e.toString());
            }
            try{
                json_data = new JSONObject(result);
                w= (CharSequence) json_data.get("re");
            }
            catch(JSONException e){
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
        }
        @Override
        protected void onPostExecute(Void Result){
            refreshItem.setActionView(null);
            LinearLayout f = null;
            if(w.equals("success")) {
                try{
                    f = (LinearLayout) findViewById(R.id.linearrecentcomplaints);
                    try{
                        f.removeAllViews();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    for(int i=0;i<json_data.length()-1;i++){
                        JSONObject no = json_data.getJSONObject(String.valueOf(i));
                        LayoutInflater I = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = I.inflate(R.layout.recentcomplaintselements,null,false);
                        TextView recentcomplaintsby = (TextView) v.findViewById(R.id.recentcomplaintsby);
                        TextView recentcomplaintstime = (TextView) v.findViewById(R.id.recentcomplaintstime);
                        TextView recentcomplaintsmessage = (TextView) v.findViewById(R.id.recentcomplaintsmessage);
                        TextView replystatus = (TextView) v.findViewById(R.id.replystatus);
                        TextView recentcomplaintsreply = (TextView) v.findViewById(R.id.recentcomplaintsreply);
                        recentcomplaintsby.setText(no.getString("Complained by"));
                        recentcomplaintstime.setText(no.getString("Time Complained"));
                        recentcomplaintsmessage.setText(no.getString("Complaint"));
                        if(!no.getString("Reply").equals("null")){
                            replystatus.setText("Replied at "+no.getString("Time Replied"));
                            replystatus.setTextColor(Color.parseColor("#00ff00"));
                            recentcomplaintsreply.setText(no.getString("Reply"));
                        }
                        else{
                            replystatus.setText("Waiting for reply");
                            replystatus.setTextColor(Color.parseColor("#ff0000"));
                            recentcomplaintsreply.setVisibility(View.GONE);
                        }
                        f.addView(v);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            else if(w.equals("Record is not available")){
            }
            else{
                connection = false;
                Toast.makeText(getApplicationContext(),"Connection failed, Please check your connection",Toast.LENGTH_SHORT).show();
            }
        }
    }

}
