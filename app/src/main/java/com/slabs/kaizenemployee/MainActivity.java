package com.slabs.kaizenemployee;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.stetho.Stetho;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences getSharedPreferences;
    SharedPreferences.Editor editSharedPreferences;

    View include_content_main;
    View include_profile;
    View include_complaint;

    CustomScrollView totalscroll;
    CustomScrollView1 contentscroll;

    String MemberId,employeeName,employeeId;

    Menu menu;

    String url;

    int NoofFailedTries=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder = Stetho.newInitializerBuilder(this);
        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this));
        // Enable command line interface
        initializerBuilder.enableDumpapp(Stetho.defaultDumperPluginsProvider(this));
        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();
        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);

        Fresco.initialize(this);

        getSharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        editSharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE).edit();
        MemberId = getSharedPreferences.getString("Member Id","0");
        employeeName = getSharedPreferences.getString("Employee Name","0");
        employeeId = getSharedPreferences.getString("Employee Pin","0");
        url = getSharedPreferences.getString("URL","192.168.42.29");
        if(MemberId.equals("0")){
            Intent login = new Intent(this, Login.class);
            this.finish();
            startActivity(login);
        }

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        TextView name = (TextView) header.findViewById(R.id.name);
        TextView id = (TextView) header.findViewById(R.id.id);
        SimpleDraweeView draweeView  = (SimpleDraweeView) header.findViewById(R.id.imageView);
        name.setText(employeeName);
        id.setText(employeeId);
        draweeView.setImageURI(Uri.parse("http://rcgu.org/images/bod/11.jpg"));

        final TextView time = (TextView) findViewById(R.id.presenttime);
        final Handler someHandler = new Handler(getMainLooper());
        someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                time.setText("Time : "+(new SimpleDateFormat("h:mm a", Locale.US).format(new Date())));
                someHandler.postDelayed(this, 1000);
            }
        }, 10);


        include_content_main = (View) findViewById(R.id.include_content_main);
        include_profile = (View) findViewById(R.id.include_profile);
        include_complaint = (View) findViewById(R.id.include_complaint);

        totalscroll = (CustomScrollView) findViewById(R.id.totalscroll);
        contentscroll = (CustomScrollView1) findViewById(R.id.contentscroll);

        Button checkin = (Button) findViewById(R.id.checkin);
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new submitAttendance().execute();
            }
        });

        Button logoutbutton = (Button) findViewById(R.id.logoutbutton);
        final Context context = this;
        logoutbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSharedPreferences.clear();
                editSharedPreferences.commit();
                Intent Login = new Intent(context, Login.class);
                finish();
                startActivity(Login);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(false);
        if(!MemberId.equals("0")){
            dashboardclicked();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            new refreshdashboard().execute();
            return true;
        }
        if (id == R.id.action_recentcomplaints) {
            Intent recentcomplaint = new Intent(this, recentComplaints.class);
            startActivity(recentcomplaint);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            dashboardclicked();
        } else if (id == R.id.nav_profile) {
            profileclicked();
        } else if (id == R.id.nav_complaint) {
            complaintclicked();
        } else if (id == R.id.nav_exit) {
            this.finish();
            System.exit(0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            totalscroll.setEnableScrolling(true);
            contentscroll.setEnableScrolling(false);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //totalscroll.setEnableScrolling(false);
            contentscroll.setEnableScrolling(true);
        }
    }

    private void dashboardclicked(){
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Dashboard");
        include_content_main.setVisibility(View.VISIBLE);
        include_profile.setVisibility(View.GONE);
        include_complaint.setVisibility(View.GONE);
        try {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        final LinearLayout f = (LinearLayout) findViewById(R.id.maincontent);
        try{
            f.removeAllViews();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        new refreshdashboard().execute();
    }

    private void profileclicked(){
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Profile");
        include_content_main.setVisibility(View.GONE);
        include_profile.setVisibility(View.VISIBLE);
        include_complaint.setVisibility(View.GONE);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        new refreshprofile().execute();
    }

    private void complaintclicked(){
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Complaint");
        include_content_main.setVisibility(View.GONE);
        include_profile.setVisibility(View.GONE);
        include_complaint.setVisibility(View.VISIBLE);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
        Button complaintsubmitbutton = (Button) findViewById(R.id.complaintsubmitbutton);
        complaintsubmitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new submitComplaint().execute();
            }
        });
    }

    private class refreshdashboard extends AsyncTask<Void,Void,Void>{

        MenuItem refreshItem;
        String result = "";
        boolean connection=false;
        JSONObject json_data;
        CharSequence w="";

        @Override
        protected Void doInBackground(Void... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            String sqlcommand = "SELECT * FROM dashboard";
            nameValuePairs.add(new BasicNameValuePair("sqlcommand",sqlcommand));

            StrictMode.setThreadPolicy(policy);

            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url+"/kaizen/accesssql.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;
                Log.e("log_tag", "connection success ");
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error in http connection "+e.toString());
            }
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null){
                    sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
            }
            catch(Exception e) {
                Log.e("log_tag", "Error converting result "+e.toString());
            }
            try{
                json_data = new JSONObject(result);
                w= (CharSequence) json_data.get("re");
            }
            catch(JSONException e){
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            refreshItem = menu.findItem(R.id.action_refresh);
            refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
        }

        @Override
        protected void onPostExecute(Void Result){
            refreshItem.setActionView(null);
            LinearLayout f;
            if(w.equals("success")) {
                try{
                    f = (LinearLayout) findViewById(R.id.maincontent);
                    try{
                        f.removeAllViews();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    for(int i=0;i<json_data.length()-1;i++){
                        JSONObject no = json_data.getJSONObject(String.valueOf(i));
                        final LayoutInflater I = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View v = I.inflate(R.layout.message,null);
                        final TextView postedBy = (TextView) v.findViewById(R.id.postedby);
                        final TextView timepostedmessage = (TextView) v.findViewById(R.id.timepostedmessage);
                        final TextView message = (TextView) v.findViewById(R.id.message);
                        postedBy.setText(no.getString("Posted by"));
                        timepostedmessage.setText(no.getString("Time Posted"));
                        message.setText(no.getString("Message"));
                        f.addView(v);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            else if(w.equals("Record is not available")){
            }
            else{
                connection = false;
                Toast.makeText(getApplicationContext(),"Connection failed, Please check your connection",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class refreshprofile extends AsyncTask<Void,Void,Void>{

        String result = "";
        boolean connection=false;
        JSONObject json_data;
        CharSequence w="";
        Dialog dialog;

        @Override
        protected Void doInBackground(Void... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            String sqlcommand = "SELECT `Name`, `Pin`, `Gender`, `Date of Birth`, `Phone Number`, `Email`, `Date of Joining` FROM employee WHERE `Member Id`='"+MemberId+"'";
            nameValuePairs.add(new BasicNameValuePair("sqlcommand",sqlcommand));

            StrictMode.setThreadPolicy(policy);

            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url+"/kaizen/accesssql.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;
                Log.e("log_tag", "connection success ");
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error in http connection "+e.toString());
            }
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null){
                    sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
            }
            catch(Exception e) {
                Log.e("log_tag", "Error converting result "+e.toString());
            }
            try{
                json_data = new JSONObject(result);
                w= (CharSequence) json_data.get("re");
            }
            catch(JSONException e){
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.attendancedialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            TextView text = (TextView) dialog.findViewById(R.id.dialogtext);
            text.setText("Processing..");
            GifImageView image = (GifImageView) dialog.findViewById(R.id.processingimage);
            image.setBackgroundResource(R.drawable.processing);
            dialog.show();
            Button declineButton = (Button) dialog.findViewById(R.id.dialogok);
            declineButton.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void Result){
            dialog.dismiss();
            if(w.equals("success")) {
                try{
                    LinearLayout f = (LinearLayout) findViewById(R.id.linear_profile_frame);
                    try{
                        f.removeAllViews();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    JSONObject no = json_data.getJSONObject("0");
                    JSONArray index = (JSONArray) no.names();


                    for(int i=0;i<json_data.getJSONObject(String.valueOf(0)).length();i++){
                        final LayoutInflater I = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View v = I.inflate(R.layout.profile_elements,null);
                        TextView profile_question = (TextView) v.findViewById(R.id.profile_question);
                        TextView profile_answer = (TextView) v.findViewById(R.id.profile_answer);
                        profile_question.setText(index.getString(i));
                        profile_answer.setText(no.getString(index.getString(i)));
                        f.addView(v);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            else{
                connection = false;
                Toast.makeText(getApplicationContext(),"Connection failed, Please check your connection",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class submitAttendance extends AsyncTask<Void,Void,Void>{
        TextView text;
        Dialog dialog;
        GifImageView image;
        Button declineButton;
        boolean connection=false;
        String result = "";
        CharSequence w="";
        Boolean validsubmission;
        JSONObject json_data;
        String wifimacaddress,wifibssid, wifiname, defaultgateway, internalip = null, externalip = null;

        @Override
        protected Void doInBackground(Void... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;
            String error = "0";

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


            try {
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                    NetworkInterface intf = en.nextElement();
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            internalip = inetAddress.getHostAddress().toString();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e("IP Address", ex.toString());
            }

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet("http://whatismyip.akamai.com/");
                // HttpGet httpget = new HttpGet("http://ipecho.net/plain");
                HttpResponse response;

                response = httpclient.execute(httpget);

                //Log.i("externalip",response.getStatusLine().toString());

                HttpEntity entity = response.getEntity();
                String externalIP = EntityUtils.toString(entity);
                if (externalIP != null) {
                    long len = externalIP.length();
                    if (len != -1 && len < 1024) {
                        String str= externalIP;
                        externalip=str;
                    } else {
                        error = "Response too long or error.";
                    }
                } else {
                    error="Null:"+response.getStatusLine().toString();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                error="Error checking IP address.";
            }

            WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = manager.getConnectionInfo();
            wifimacaddress = wifiInfo.getMacAddress();
            wifibssid = "1234";
            wifiname = "banana";
            defaultgateway = "192.168.1.1";
            //wifibssid = wifiInfo.getBSSID();
            //wifiname = wifiInfo.getSSID();
            //defaultgateway = String.valueOf(manager.getDhcpInfo().gateway);

            if(error.equals("0")) {
                String operation = "INSERT INTO `attendance` (`Pin`,`Time Attended`,`Time on Phone`,`MAC Address`,`Internal IP Address`,`External IP Address`,`Logged in as`,`Failed tries`) VALUES('" + getSharedPreferences.getString("Employee Pin", "0") + "',NULL,'" + new SimpleDateFormat("yyyy-MM-dd H:mm:ss", Locale.US).format(new Date()) + "','"+wifimacaddress+"','" + internalip + "','" + externalip + "','"+getSharedPreferences.getString("Employee Name","0")+"','"+getSharedPreferences.getString("Failed tries","0")+"')";
                String condition = "SELECT COUNT(*) AS authenticated FROM `authentication` WHERE `Wifi Name`='"+wifiname+"' AND `Wifi MAC Address`='"+wifibssid+"' AND `IPv4 Default Gateway`='"+defaultgateway+"' AND `id`=(SELECT MAX(`id`) FROM `authentication`)";
                String sqlcommand = "SELECT * FROM `authentication` WHERE `id`=(SELECT MAX(`id`) FROM `authentication`)";
                nameValuePairs.add(new BasicNameValuePair("sqlcommand", sqlcommand));
                nameValuePairs.add(new BasicNameValuePair("operation", operation));
                nameValuePairs.add(new BasicNameValuePair("condition", condition));

                StrictMode.setThreadPolicy(policy);

                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(url + "/kaizen/accesssql.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    connection = true;
                    Log.e("log_tag", "connection success ");
                } catch (Exception e) {
                    Log.e("log_tag", "Error in http connection " + e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                } catch (Exception e) {
                    Log.e("log_tag", "Error converting result " + e.toString());
                }
                try {
                    json_data = new JSONObject(result);
                    w = (CharSequence) json_data.get("re");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("log_tag", "Error parsing data " + e.toString());
                    Log.e("log_tag", "Failed data was:\n" + result);
                }
            }
            return null;

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.attendancedialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            text = (TextView) dialog.findViewById(R.id.dialogtext);
            text.setText("Processing..");
            image = (GifImageView) dialog.findViewById(R.id.processingimage);
            image.setBackgroundResource(R.drawable.processing);
            dialog.show();
            declineButton = (Button) dialog.findViewById(R.id.dialogok);
            declineButton.setVisibility(View.GONE);
            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
        @Override
        protected void onPostExecute(Void Result){
            if(w.equals("success")) {
                image.setBackgroundResource(R.drawable.checkmark);
                text.setText("Successfully Submitted");
                declineButton.setVisibility(View.VISIBLE);
                editSharedPreferences.putString("Failed tries","");
                NoofFailedTries=0;
                editSharedPreferences.commit();
                try {
                    JSONObject no = json_data.getJSONObject("0");
                    editSharedPreferences.putString("Wifi Name",no.getString("Wifi Name"));
                    editSharedPreferences.putString("Wifi MAC Address",no.getString("Wifi MAC Address"));
                    editSharedPreferences.putString("IPv4 Default Gateway",no.getString("IPv4 Default Gateway"));
                    editSharedPreferences.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if(w.equals("DifferentWifi")){
                image.setBackgroundResource(R.drawable.failed);
                text.setText("Not authenticated with Office Wifi.\nContact Admin.");
                declineButton.setVisibility(View.VISIBLE);
                editSharedPreferences.putString("Failed tries",getSharedPreferences.getString("Failed tries","")+String.valueOf(NoofFailedTries)+" "+new SimpleDateFormat("yyyy-MM-dd H:mm:ss", Locale.US).format(new Date())+" DifferentWifi "+wifiname+" "+wifibssid+" "+defaultgateway+" Int.IP:"+internalip+" Ext.IP:"+externalip+"\n");
                NoofFailedTries++;
                editSharedPreferences.commit();
            }
            else{
                image.setBackgroundResource(R.drawable.failed);
                text.setText("Submission failed, Try again..");
                declineButton.setVisibility(View.VISIBLE);
                editSharedPreferences.putString("Failed tries",getSharedPreferences.getString("Failed tries","")+String.valueOf(NoofFailedTries)+" "+new SimpleDateFormat("yyyy-MM-dd H:mm:ss", Locale.US).format(new Date())+" OfficeWifi "+wifiname+" "+wifibssid+" "+defaultgateway+" Int.IP:"+internalip+" Ext.IP:"+externalip+"\n");
                NoofFailedTries++;
                editSharedPreferences.commit();
            }
        }
    }

    private class submitComplaint extends AsyncTask<Void,Void,Void>{
        TextView text;
        Dialog dialog;
        GifImageView image;
        Button declineButton;
        boolean connection=false;
        String result = "";
        String message;
        String complainttype;
        CharSequence w="";

        @Override
        protected Void doInBackground(Void... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            String sqlcommand = "INSERT INTO `complaint` (`Complained by`, `Complaint`, `Type`) VALUES('"+employeeId+"','"+message+"','"+complainttype+"')";
            nameValuePairs.add(new BasicNameValuePair("sqlcommand",sqlcommand));

            StrictMode.setThreadPolicy(policy);

            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url+"/kaizen/accesssql.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;
                Log.e("log_tag", "connection success ");
            }
            catch(Exception e){
                Log.e("log_tag", "Error in http connection "+e.toString());
            }
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null){
                    sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
            }
            catch(Exception e) {
                Log.e("log_tag", "Error converting result "+e.toString());
            }
            try{
                JSONObject json_data = new JSONObject(result);
                w= (CharSequence) json_data.get("re");
            }
            catch(JSONException e){
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.attendancedialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            text = (TextView) dialog.findViewById(R.id.dialogtext);
            text.setText("Processing..");
            image = (GifImageView) dialog.findViewById(R.id.processingimage);
            image.setBackgroundResource(R.drawable.processing);
            dialog.show();
            declineButton = (Button) dialog.findViewById(R.id.dialogok);
            declineButton.setVisibility(View.GONE);
            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            LayoutInflater I = getLayoutInflater();
            ViewGroup view1 = (ViewGroup) findViewById(android.R.id.content);
            View vv = I.inflate(R.layout.complaint, view1, true);
            EditText complaintmessage = (EditText) vv.findViewById(R.id.complaintmessage);
            message = complaintmessage.getText().toString();
            RadioGroup complainttyperadio = (RadioGroup) vv.findViewById(R.id.complainttyperadio);
            int idx = complainttyperadio.indexOfChild(complainttyperadio.findViewById(complainttyperadio.getCheckedRadioButtonId()));
            RadioButton r = (RadioButton) complainttyperadio.getChildAt(idx);
            complainttype = r.getText().toString();
            view1.removeViewAt(1);
        }
        @Override
        protected void onPostExecute(Void Result){
            if(w.equals("success")) {
                image.setBackgroundResource(R.drawable.checkmark);
                text.setText("Successfully Submitted");
                declineButton.setVisibility(View.VISIBLE);
            }
            else{
                image.setBackgroundResource(R.drawable.failed);
                text.setText("Submission failed, Try again..");
                declineButton.setVisibility(View.VISIBLE);
            }
        }

    }
}
