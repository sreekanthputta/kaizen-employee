package com.slabs.kaizenemployee;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Sreekanth Putta on 25-10-2016.
 */
public class Login extends Activity{
    TextInputLayout input_layout_username, input_layout_password;
    EditText loginusername, loginpassword, loginhostname;
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        input_layout_username = (TextInputLayout) findViewById(R.id.input_layout_username);
        input_layout_password = (TextInputLayout) findViewById(R.id.input_layout_password);
        loginusername = (EditText) findViewById(R.id.loginusername);
        loginpassword = (EditText) findViewById(R.id.loginpassword);
        loginhostname = (EditText) findViewById(R.id.loginhostname);
        loginusername.addTextChangedListener(new MyTextWatcher(loginusername));
        loginpassword.addTextChangedListener(new MyTextWatcher(loginpassword));
        Button loginbutton = (Button) findViewById(R.id.loginbutton);

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginusername.getText().length()==0){
                    input_layout_username.setError("Enter a valid Username");
                }
                else if(loginpassword.getText().length()==0){
                    input_layout_password.setError("Enter a valid Password");
                }
                else{
                    new login().execute();
                }
            }
        });

    }

    private void gotomainactivity() {
        Intent mainactivity = new Intent(this, MainActivity.class);
        this.finish();
        startActivity(mainactivity);
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.loginusername:
                    input_layout_username.setErrorEnabled(false);
                    break;
                case R.id.loginpassword:
                    input_layout_password.setErrorEnabled(false);
                    break;
            }
        }
    }

    private class login extends AsyncTask<Void,Void,Void>{

        String result = "";
        String employeeName, employeePin, memberId;
        boolean connection=false, login=true;
        Dialog dialog;
        String username, password;
        CharSequence w="";
        @Override
        protected Void doInBackground(Void... params) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            InputStream is = null;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = manager.getConnectionInfo();
            String wifimacaddress = wifiInfo.getMacAddress();

            String loginoperation = "SELECT * FROM `employee` WHERE `Pin`='"+username+"' AND `Password`='"+password+"' AND `MAC Address`='"+wifimacaddress+"'";
            String logincondition = "SELECT `MAC Address` FROM `employee` WHERE `Pin`='"+username+"' AND `Password`='"+password+"'";
            String loginsqlcommand = "UPDATE `employee` SET `MAC Address`='"+wifimacaddress+"' WHERE `Pin`='"+username+"' AND `Password`='"+password+"'";
            nameValuePairs.add(new BasicNameValuePair("loginsqlcommand", loginsqlcommand));
            nameValuePairs.add(new BasicNameValuePair("loginoperation", loginoperation));
            nameValuePairs.add(new BasicNameValuePair("logincondition", logincondition));

            StrictMode.setThreadPolicy(policy);

            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url+"/kaizen/accesssql.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                connection = true;
                Log.e("log_tag", "connection success ");
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error in http connection "+e.toString());
            }
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null){
                    sb.append(line + "\n");
                }
                is.close();
                result=sb.toString();
            }
            catch(Exception e) {
                Log.e("log_tag", "Error converting result "+e.toString());
            }
            try{
                JSONObject json_data = new JSONObject(result);
                w= (CharSequence) json_data.get("re");
                if(w.equals("success")||w.equals("updated")) {
                    JSONObject no = json_data.getJSONObject("0");
                    employeeName = no.getString("Name");
                    employeePin = no.getString("Pin");
                    memberId = no.getString("Member Id");
                }
                else{
                    login = false;
                }
            }
            catch(JSONException e){
                e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
                Log.e("log_tag", "Failed data was:\n" + result);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(Login.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.attendancedialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.findViewById(R.id.dialogok).setVisibility(View.GONE);
            TextView text = (TextView) dialog.findViewById(R.id.dialogtext);
            text.setText("Processing");
            GifImageView image = (GifImageView) dialog.findViewById(R.id.processingimage);
            image.setBackgroundResource(R.drawable.processing);
            dialog.show();
            username = loginusername.getText().toString();
            password = loginpassword.getText().toString();

            if(loginhostname.getText().toString().length()!=0){
                url = "http://"+loginhostname.getText().toString();
            }
            else{
                url = "http://192.168.42.29";
            }
        }

        @Override
        protected void onPostExecute(Void Result) {
            // dismiss the dialog once done
            dialog.dismiss();
            if(connection == false){
                Toast.makeText(getApplicationContext(),"Connection failed, Please check your connection",Toast.LENGTH_SHORT).show();
            }
            else if(login == false){
                Toast.makeText(getApplicationContext(),"Username and Password doesn't match",Toast.LENGTH_SHORT).show();
            }
            else {
                if(w.equals("success")){
                    Toast.makeText(getApplicationContext(),"Logged in successfully",Toast.LENGTH_SHORT).show();
                }
                else if(w.equals("updated")){
                    Toast.makeText(getApplicationContext(),"Device registered and logged in successfully",Toast.LENGTH_SHORT).show();
                }
                SharedPreferences.Editor sharedpreferences;
                sharedpreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE).edit();
                sharedpreferences.putString("Employee Name", employeeName);
                sharedpreferences.putString("Employee Pin", employeePin);
                sharedpreferences.putString("Member Id", memberId);
                sharedpreferences.putString("URL", url);
                sharedpreferences.commit();
                gotomainactivity();
            }
        }
    }
}
